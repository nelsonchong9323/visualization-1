document.addEventListener("DOMContentLoaded", function (event) {
    btnOperation();
    followerCelcom();
    followerDigi();
    hashtag();
    mention();
    repliesComparison();
    topRetweets();
    opinionIndustry();
    brandCelcom();
    brandDigi();
});

window.addEventListener("resize", function (event) {
    followerCelcom();
    followerDigi();
    hashtag();
    mention();
    repliesComparison();
    topRetweets();
    opinionIndustry();
    brandCelcom();
    brandDigi();
});

var btnOperation = function () {
    $('#btnassg1').on('click', function() {
        $('#assg1').toggleClass('show hide');
    });
    
    $('#btnassg2').on('click', function() {
        $('#assg2').toggleClass('show hide');
    });
    
    $('#btnassg3').on('click', function() {
        $('#assg3').toggleClass('show hide');
    });
}

var followerCelcom = function () {
    $("#celcomFollower").empty();
    var margin = {
            top: 20,
            right: 20,
            bottom: 30,
            left: 50
        },
        width = $("#celcomFollower").width() - margin.left - margin.right,
        height = 300 - margin.top - margin.bottom;

    var parseTime = d3v4.timeParse("%m/%d/%Y");

    var x = d3v4.scaleTime().range([0, width]);
    var y = d3v4.scaleLinear().range([height, 0]);

    var valueline = d3v4.line()
        .x(function (d) {
            return x(d.date);
        })
        .y(function (d) {
            return y(d.count);
        });


    var svg = d3v4.select("#celcomFollower").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    d3v4.csv("SortedData/celcomfollowersupdated.csv", function (error, data) {
        if (error) throw error;

        data.forEach(function (d) {
            d.date = parseTime(d.date);
            d.count = +d.count;
            //console.log(d.date, d.count);
        });

        x.domain(d3v4.extent(data, function (d) {
            return d.date;
        }));
        y.domain([52000, d3v4.max(data, function (d) {
            return d.count;
        }) + 1000]).range[500, 0];

        svg.append("text")
            .attr("x", (width / 2))
            .attr("y", 0 - (margin.top / 4))
            .attr("text-anchor", "middle")
            .style("font-size", "20px")
            .style('fill', 'SteelBlue')
            .text("Celcom Audience Growth");

        svg.append("path")
            .data([data])
            .attr("class", "line celcom")
            .attr("d", valueline);


        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3v4.axisBottom(x));


        svg.append("g")
            .call(d3v4.axisLeft(y));

    });

}

var followerDigi = function () {
    $("#digiFollower").empty();
    var margin = {
            top: 20,
            right: 20,
            bottom: 30,
            left: 50
        },
        width = $("#digiFollower").width() - margin.left - margin.right,
        height = 300 - margin.top - margin.bottom;


    var parseTime = d3v4.timeParse("%m/%d/%Y");


    var x = d3v4.scaleTime().range([0, width]);
    var y = d3v4.scaleLinear().range([height, 0]);

    var valueline = d3v4.line()
        .x(function (d) {
            return x(d.date);
        })
        .y(function (d) {
            return y(d.count);
        });


    var svg = d3v4.select("#digiFollower").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");


    d3v4.csv("SortedData/digifollowersupdated.csv", function (error, data) {
        if (error) throw error;


        data.forEach(function (d) {
            d.date = parseTime(d.date);
            d.count = +d.count;
            //console.log(d.date, d.count);
        });


        x.domain(d3v4.extent(data, function (d) {
            return d.date;
        }));
        y.domain([160000, d3v4.max(data, function (d) {
            return d.count;
        }) + 2500]).range[500, 0];
        var growth = (d3v4.max(data, function (d) {
            return d.count;
        }) / d3v4.min(data, function (d) {
            return d.count;
        })) * 100;
        console.log("percent:", growth, "%");
        svg.append("text")
            .attr("x", (width / 2))
            .attr("y", 0 - (margin.top / 4))
            .attr("text-anchor", "middle")
            .style("font-size", "20px")
            .style('fill', 'Gold')
            .text("Digi Audience Growth");

        svg.append("path")
            .data([data])
            .attr("class", "line digi")
            .attr("d", valueline);


        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3v4.axisBottom(x));


        svg.append("g")
            .call(d3v4.axisLeft(y));

    });

}

var hashtag = function () {
    $("#hashtag").empty();
    var margin = {
            top: 20,
            right: 20,
            bottom: 30,
            left: 40
        },
        width = $("#hashtag").width() - margin.left - margin.right,
        height = 400 - margin.top - margin.bottom;

    var x = d3v4.scaleBand()
        .range([0, width / 1.5])
        .padding(0.1);
    var y = d3v4.scaleLinear()
        .range([height, 0]);


    var svg = d3v4.select("#hashtag").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");


    d3v4.csv("SortedData/hashtag.csv", function (error, data) {
        if (error) throw error;


        data.forEach(function (d) {
            d.count = +d.count;
        });


        x.domain(data.map(function (d) {
            return d.hashtag;
        }));
        y.domain([0, d3v4.max(data, function (d) {
            return d.count;
        }) + 1]);


        svg.selectAll(".bar")
            .data(data)
            .enter().append("rect")
            .attr("class", "bar")
            .attr("x", function (d) {
                return x(d.hashtag);
            })
            .attr("width", x.bandwidth())
            .attr("fill", function (d) {
                if (d.hashtag == "CelcomKindnessChallenge") {
                    return d.color = "steelblue";
                } else {
                    return d.color = "gold";
                }
            })
            .attr("y", function (d) {
                return y(d.count);
            })
            .attr("height", function (d) {
                return height - y(d.count);
            });


        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3v4.axisBottom(x));


        svg.append("g")
            .call(d3v4.axisLeft(y));

        svg.append("g")
            .attr("transform", "translate(" + (width / 2) + ", 15)")
            .append("text")
            .text("SMC Hashtags")
            .style({
                "text-anchor": "middle",
                "font-family": "Arial",
                "font-weight": "800"
            });

    });

}

var mention = function () {
    $("#mention").empty();
    var margin = {
            top: 20,
            right: 20,
            bottom: 30,
            left: 40
        },
        width = $("#mention").width() - margin.left - margin.right,
        height = 400 - margin.top - margin.bottom;


    var x = d3v4.scaleBand()
        .range([0, width / 1.5])
        .padding(0.1);
    var y = d3v4.scaleLinear()
        .range([height, 0]);


    var svg = d3v4.select("#mention").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

    d3v4.csv("SortedData/mention.csv", function (error, data) {
        if (error) throw error;


        data.forEach(function (d) {
            d.count = +d.count;
        });


        x.domain(data.map(function (d) {
            return d.user;
        }));
        y.domain([0, d3v4.max(data, function (d) {
            return d.count;
        }) + 500]);


        svg.selectAll(".bar")
            .data(data)
            .enter().append("rect")
            .attr("class", "bar")
            .attr("x", function (d) {
                return x(d.user);
            })
            .attr("width", x.bandwidth())
            .attr("fill", function (d) {
                if (d.user == "Celcom") {
                    return d.color = "steelblue";
                } else {
                    return d.color = "gold";
                }
            })
            .attr("y", function (d) {
                return y(d.count);
            })
            .attr("height", function (d) {
                return height - y(d.count);
            });


        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3v4.axisBottom(x));


        svg.append("g")
            .call(d3v4.axisLeft(y));

        svg.append("g")
            .attr("transform", "translate(" + (width / 2) + ", 15)")
            .append("text")
            .text("User Mentions")
            .style({
                "text-anchor": "middle",
                "font-family": "Arial",
                "font-weight": "800"
            });

    });

}

var repliesComparison = function () {
    $("#repliesComparison").empty();
    var margin = {
            top: 30,
            right: 20,
            bottom: 70,
            left: 50
        },
        width = $("#repliesComparison").width() - margin.left - margin.right,
        height = 400 - margin.top - margin.bottom;


    var parseDate = d3v3.time.format("%d-%b-%y").parse;

    // Set the ranges
    var x = d3v3.time.scale().range([0, width]);
    var y = d3v3.scale.linear().range([height, 0]);

    var xAxis = d3v3.svg.axis().scale(x)
        .orient("bottom").ticks(5);

    var yAxis = d3v3.svg.axis().scale(y)
        .orient("left").ticks(5);


    var priceline = d3v3.svg.line()
        .x(function (d) {
            return x(d.date);
        })
        .y(function (d) {
            return y(d.count);
        });


    var svg = d3v3.select("#repliesComparison")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


    d3v3.csv("SortedData/replies.csv", function (error, data) {
        data.forEach(function (d) {
            d.date = parseDate(d.date);
            d.count = +d.count;
            //console.log(d.date, d.count);
        });

        x.domain(d3v3.extent(data, function (d) {
            return d.date;
        }));
        y.domain([0, d3v3.max(data, function (d) {
            return d.count;
        })]);

        var dataNest = d3v3.nest()
            .key(function (d) {
                return d.user;
            })
            .entries(data);

        var color = d3v3.scale.category10();

        legendSpace = width / dataNest.length;

        dataNest.forEach(function (d, i) {
            svg.append("path")
                .attr("class", "line")
                .style("stroke", function () {
                    if (d.key == "celcom") {
                        return d.color = "steelblue";
                    } else {
                        return d.color = "gold";
                    }
                })
                .attr("d", priceline(d.values));

            svg.append("text")
                .attr("x", (legendSpace / 2) + i * legendSpace)
                .attr("y", height + (margin.bottom / 2) + 5)
                .attr("class", "legend")
                .style("stroke", function () {
                    if (d.key == "celcom") {
                        return d.color = "steelblue";
                    } else {
                        return d.color = "gold";
                    }
                })
                .text(d.key);
        });
        
        svg.append("text")
            .attr("x", (width / 2))
            .attr("y", 0 - (margin.top / 2))
            .attr("text-anchor", "middle")
            .style("font-size", "16px")
            .text("User Replies Comparison");

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);

        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis);
    });
}

var topRetweets = function () {
    $("#topRetweets").empty();
    d3v3.text("SortedData/topretweet.csv", function (data) {
        var parsedCSV = d3v3.csv.parseRows(data);

        var container = d3v3.select("#topRetweets")
            .append("table")
            .selectAll("tr")
            .data(parsedCSV).enter()
            .append("tr")
            .selectAll("td")
            .data(function (d) {
                return d;
            }).enter()
            .append("td")
            .text(function (d) {
                return d;
            });
    });
}

var opinionIndustry = function () {
    $("#opinion").empty();
    var margin = {
            top: 20,
            right: 20,
            bottom: 30,
            left: 40
        },
        width = $("#opinion").width() - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;

    var x = d3v4.scaleBand()
        .range([0, width])
        .padding(0.1);
    var y = d3v4.scaleLinear()
        .range([height, 0]);

    var svg = d3v4.select("#opinion").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

    d3v4.csv("SortedData/opinion_industry.csv", function (error, data) {
        if (error) throw error;

        data.forEach(function (d) {
            d.percentage = +d.percentage;
        });

        x.domain(data.map(function (d) {
            return d.opinion;
        }));
        y.domain([0, d3v4.max(data, function (d) {
            return d.percentage;
        }) + 40]);

        svg.selectAll(".bar")
            .data(data)
            .enter().append("rect")
            .attr("class", "bar")
            .attr("x", function (d) {
                return x(d.opinion);
            })
            .attr("width", x.bandwidth())
            .attr("fill", function (d) {
                if (d.opinion == "Positive_Tweets") {
                    return d.color = "steelblue";
                } else {
                    return d.color = "crimson";
                }
            })
            .attr("y", function (d) {
                return y(d.percentage);
            })
            .attr("height", function (d) {
                return height - y(d.percentage);
            });

        svg.append("g")
            .attr("transform", "translate(10," + height + ")")
            .style("font-size", "20px")
            .call(d3v4.axisBottom(x));

        svg.append("g")
            .style("font-size", "20px")
            .call(d3v4.axisLeft(y));
    });
}

var brandCelcom = function () {
    $("#brandcelcom").empty();
    var margin = {
            top: 20,
            right: 20,
            bottom: 30,
            left: 40
        },
        width = $("#brandcelcom").width() - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;

    var x = d3v4.scaleBand()
        .range([0, width])
        .padding(0.1);
    var y = d3v4.scaleLinear()
        .range([height, 0]);

    var svg = d3v4.select("#brandcelcom").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

    d3v4.csv("SortedData/brand_celcom.csv", function (error, data) {
        if (error) throw error;

        data.forEach(function (d) {
            d.percentage = +d.percentage;
        });

        x.domain(data.map(function (d) {
            return d.sentiment;
        }));
        y.domain([0, d3v4.max(data, function (d) {
            return d.percentage;
        }) + 40]);

        svg.selectAll(".bar")
            .data(data)
            .enter().append("rect")
            .attr("class", "bar")
            .attr("x", function (d) {
                return x(d.sentiment);
            })
            .attr("width", x.bandwidth())
            .attr("fill", function (d) {
                if (d.sentiment == "Positive") {
                    return d.color = "steelblue";
                } else {
                    return d.color = "crimson";
                }
            })
            .attr("y", function (d) {
                return y(d.percentage);
            })
            .attr("height", function (d) {
                return height - y(d.percentage);
            });

        svg.append("g")
            .attr("transform", "translate(10," + height + ")")
            .style("font-size", "20px")
            .call(d3v4.axisBottom(x));

        svg.append("g")
            .style("font-size", "20px")
            .call(d3v4.axisLeft(y));
        
        svg.append("g")
            .attr("transform", "translate(" + (width / 2) + ", 15)")
            .append("text")
            .text("Celcom")
            .style({
                "text-anchor": "middle",
                "font-family": "Arial",
                "font-size": "50px"
            });
    });
}

var brandDigi = function () {
    $("#branddigi").empty();
    var margin = {
            top: 20,
            right: 20,
            bottom: 30,
            left: 40
        },
        width = $("#branddigi").width() - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;

    var x = d3v4.scaleBand()
        .range([0, width])
        .padding(0.1);
    var y = d3v4.scaleLinear()
        .range([height, 0]);

    var svg = d3v4.select("#branddigi").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

    d3v4.csv("SortedData/brand_digi.csv", function (error, data) {
        if (error) throw error;

        data.forEach(function (d) {
            d.percentage = +d.percentage;
        });

        x.domain(data.map(function (d) {
            return d.sentiment;
        }));
        y.domain([0, d3v4.max(data, function (d) {
            return d.percentage;
        }) + 40]);

        svg.selectAll(".bar")
            .data(data)
            .enter().append("rect")
            .attr("class", "bar")
            .attr("x", function (d) {
                return x(d.sentiment);
            })
            .attr("width", x.bandwidth())
            .attr("fill", function (d) {
                if (d.sentiment == "Positive") {
                    return d.color = "steelblue";
                } else {
                    return d.color = "crimson";
                }
            })
            .attr("y", function (d) {
                return y(d.percentage);
            })
            .attr("height", function (d) {
                return height - y(d.percentage);
            });

        svg.append("g")
            .attr("transform", "translate(10," + height + ")")
            .style("font-size", "20px")
            .call(d3v4.axisBottom(x));

        svg.append("g")
            .style("font-size", "20px")
            .call(d3v4.axisLeft(y));
        
        svg.append("g")
            .attr("transform", "translate(" + (width / 2) + ", 15)")
            .append("text")
            .text("Digi")
            .style({
                "text-anchor": "middle",
                "font-family": "Arial",
                "font-size": "50px"
            });
    });
}